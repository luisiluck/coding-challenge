package com.truenorth.internal.qa_automation.web;

import com.truenorth.internal.qa_automation.utils.PropertyUtils;
import com.truenorth.internal.qa_automation.web.pages.SearchResultsPage;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

/**
 * Coding Challenge
 *
 * WebTests Suite
 */
@RunWith(Parameterized.class)
public class WebTests extends BaseTest
{

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { 90 }, { 100 }, { 110 }, { 120 }, { 130 }
        });
    }

    private int daysFromNow;

    public WebTests(int daysFromNow) {
        this.daysFromNow = daysFromNow;
    }

    @Test
    public void test() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, daysFromNow);
        Date dateFrom = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, 2);
        Date dateTo = calendar.getTime();
        SearchResultsPage oneWaySearch = new SearchResultsPage(driver,
                PropertyUtils.getWebLocationFirst(),
                dateFrom, dateTo,
                "price_a");
        SearchResultsPage twoWaySearch = new SearchResultsPage(driver,
                PropertyUtils.getWebLocationFirst(),
                PropertyUtils.getWebLocationSecond(),
                dateFrom, dateTo,
        "price_a");
        Assert.assertTrue(
                String.format("%s%n%s", oneWaySearch, twoWaySearch),
                oneWaySearch.getFirstItemPrice()<twoWaySearch.getFirstItemPrice());
        logger.info(String.format("%s%n%s", oneWaySearch, twoWaySearch));
    }
}
