package com.truenorth.internal.qa_automation.api;

import com.truenorth.internal.qa_automation.api.model.Response;
import com.truenorth.internal.qa_automation.utils.ApiUtils;
import org.junit.Assert;
import org.junit.Test;

import static io.restassured.RestAssured.given;

public class ApiTests extends BaseTest{

    @Test
    public void test() {
        Response response = given()
                .queryParam("language", "sus")
                .queryParam("limit", "1000")
                .queryParam("q", "tor")
                .when().get().as(Response.class);
        Assert.assertTrue(response.getDestinations().size() >= 2);
        Assert.assertTrue(response.getCountries().size() >= 2);
        ApiUtils.initDestinations(response);
    }
}
