package com.truenorth.internal.qa_automation.web.pages;

import org.openqa.selenium.WebDriver;

abstract class BasePage {

    private WebDriver driver;

    BasePage(WebDriver driver) {
        this.driver = driver;
    }

    WebDriver getDriver() {
        return driver;
    }
}
