package com.truenorth.internal.qa_automation.web.pages;

import com.truenorth.internal.qa_automation.exceptions.InitException;
import com.truenorth.internal.qa_automation.utils.PropertyUtils;
import com.truenorth.internal.qa_automation.utils.WebUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;

public class SearchResultsPage extends BasePage {

    private String start;
    private String end;
    private String location;
    private long firstItemPrice;
    private By itemPrice = By.cssSelector("[class='_eX _wN']");

    public SearchResultsPage(WebDriver driver, String location1, String location2, Date start, Date end, String sorting) {
        super(driver);
        this.start = WebUtils.dateToString(start);
        this.end = WebUtils.dateToString(end);
        this.location = "".equals(location2) ? location1 : location1 + "/" + location2;
        try {
            URI uri = WebUtils.createUri(PropertyUtils.getWebUri())
                    .setPath(String.format("%s/%s/%s/%s",
                            PropertyUtils.getWebPathCars(),
                            this.location ,this.start, this.end))
                    .addParameter("sort", sorting).build();
            driver.get(uri.toString());
            getFirstItemPrice();
        } catch (URISyntaxException e1) {
            throw new InitException(e1);
        }
    }

    public SearchResultsPage(WebDriver driver, String location1, Date start, Date end, String sorting) {
        this(driver, location1, "", start, end, sorting);
    }

    public long getFirstItemPrice() {
        if(firstItemPrice == 0) {
            firstItemPrice = Long.parseLong(itemPrice.findElement(getDriver()).getText()
                    .replaceAll("[^\\d]", ""));
        }
        return firstItemPrice;
    }

    @Override
    public String toString() {
        return String.format(
                "Search parameters:%n Dates: %s/%s%nLocations: %s%nFirst item price: %d",
                start, end, location, firstItemPrice);
    }
}
