package com.truenorth.internal.qa_automation.utils;

import com.truenorth.internal.qa_automation.exceptions.InitException;
import org.apache.http.client.utils.URIBuilder;

import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class WebUtils {

    /**
     *
     * @param date to transform
     * @return date as String in web query format
     */
    public static String dateToString(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(date);
    }

    public static URIBuilder createUri(String uri) {
        try {
            return new URIBuilder(uri);
        } catch (URISyntaxException e) {
            throw new InitException(e);
        }
    }

}
