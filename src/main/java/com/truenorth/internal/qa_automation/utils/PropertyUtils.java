package com.truenorth.internal.qa_automation.utils;

import com.truenorth.internal.qa_automation.exceptions.InitException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public abstract class PropertyUtils {

    private static Properties config;

    public static Properties getProperties() {
        if(config == null) {
            config = new Properties();
            InputStream conf = Thread.currentThread().getContextClassLoader()
                    .getResourceAsStream("config.properties");
            try {
                config.load(conf);
            } catch (IOException e) {
                throw new InitException(e);
            }
        }
        return config;
    }

    public static String getApiUri(){
        return getProperties().getProperty("API_URI");
    }

    public static String getWebUri(){
        return getProperties().getProperty("WEB_URI");
    }

    public static String getWebPathCars(){
        return getProperties().getProperty("WEB_PATH_CARS");
    }

    public static String getApiPathDestinations(){
        return getProperties().getProperty("API_PATH_DESTINATIONS");
    }

    public static String getWebLocationFirst() {
        return getProperties().getProperty("WEB_LOCATION_FIRST");
    }

    public static String getWebLocationSecond() {
        return getProperties().getProperty("WEB_LOCATION_SECOND");
    }
}
