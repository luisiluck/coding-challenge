package com.truenorth.internal.qa_automation.utils;

import com.truenorth.internal.qa_automation.api.model.Response;

import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class ApiUtils {

    private static Logger logger = Logger.getAnonymousLogger();

    public static void initDestinations(Response response) {
        response.getCountries().forEach(
                country -> response.getDestinations()
                        .forEach(destination -> {
                            if(destination.getCountry_id().equals(country.getId()))
                                country.getDestinations().add(destination);
                        }));
        logger.log(Level.INFO,"Countries\n{0}",response.getCountries());
    }
}
