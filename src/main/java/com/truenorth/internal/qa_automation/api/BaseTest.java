package com.truenorth.internal.qa_automation.api;

import com.truenorth.internal.qa_automation.utils.PropertyUtils;
import io.restassured.RestAssured;

import org.junit.BeforeClass;


public abstract class BaseTest {

    @BeforeClass
    public static void setup() {
        RestAssured.baseURI = PropertyUtils.getApiUri();
        RestAssured.basePath = PropertyUtils.getApiPathDestinations();
    }
}
