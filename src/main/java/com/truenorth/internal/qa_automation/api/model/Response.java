
package com.truenorth.internal.qa_automation.api.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "meta",
    "destinations",
    "countries"
})
public class Response {

    @JsonProperty("meta")
    private Meta meta;
    @JsonProperty("destinations")
    private List<Destination> destinations = null;
    @JsonProperty("countries")
    private List<Country> countries = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("meta")
    public Meta getMeta() {
        return meta;
    }

    @JsonProperty("meta")
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    @JsonProperty("destinations")
    public List<Destination> getDestinations() {
        return destinations;
    }

    @JsonProperty("destinations")
    public void setDestinations(List<Destination> destinations) {
        this.destinations = destinations;
    }

    @JsonProperty("countries")
    public List<Country> getCountries() {
        return countries;
    }

    @JsonProperty("countries")
    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
