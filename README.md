# Coding Challenge

## Requisites

* git
* JDK  1.8_X
* Maven

## Instructions

1. git clone https://bitbucket.org/luisiluck/coding-challenge.git
2. run ``` mvn -Dtest=<type_of_Test>  clean install``` where *type_of_Test*...
    * ```WebTests``` for UI test. Kayak scenario. WARNING: Web page has robot protection.
    * ```ApiTests``` for API test. carrentals.com scenario

**Example**
``` 
mvn -Dtest=WebTests clean install
```
